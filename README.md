![HA](assets/ha.png)

# My Home Assistant Config

My Home Assistant Config Notes

## SETUP ##
* python 3.7.4
* v0.102.2 by pip
* mosquitto *mqtt*

## START ##
* `hass --open-io`

## COMPONENTS ##
* [HACS](https://github.com/hacs/integration)


## PLUGIN ##
* [Bar Card](https://github.com/custom-cards/bar-card)
* [Custom Header](https://github.com/maykar/custom-header)
* [Mini Graph Card](https://github.com/kalkih/mini-graph-card)
* Notification on Slack


## RESOURCES ##
* [hadevices](https://www.hadevices.com/)
* [flic-1](https://ethitter.com/2016/12/flic-buttons-and-home-assistant/)
* [flic-2](https://www.home-assistant.io/integrations/flic/)
* [Conf.Bella #aFFekopp](https://github.com/aFFekopp/homeassistant)
* [altra conf carina #rniemand](https://github.com/rniemand/home-assistant)
* [conf bella #adonno](https://github.com/adonno/Home-AssistantConfig)
* [Sonoff Mini DIY](https://lamiacasaelettrica.com/sonoff-mini/)


## HARDWARE ##
* [1 x Meross Smart Thermostat](https://www.amazon.it/Termostatica-Intelligente-Programmabile-Controllo-Compatibile/dp/B08D7C9Y5F/) [github](https://github.com/albertogeniola/meross-homeassistant)
* 2 x Tuya Light Switch
* 2 x Tuya Prese Smart
* 1 x TPLink Presa Smart
* 1 x Sonoff Mini [foto](assets/sonoff.jpeg)
* 4 x Shelly 1PM (da comprare) Lavastoviglie / Forno / Frigorifero / Lavabiancheria **15 euro**
* 10 x Shelly 1v3 (da comprare) Cucina / Bagno 1-2 / Salotto / Camera 1-2-3 **10 euro** [foto](assets/shelly.jpeg)

### DA COMPRARE ###
